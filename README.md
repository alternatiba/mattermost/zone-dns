# Configuration zone DNS alternatiba.eu pour Mattermost

URL de l'instance : chat.alternatiba.eu

Config DNS :
```
chat                   3600  IN CNAME  mm.alternatiba.girofle.cloud.
mail._domainkey.chat   3600  IN CNAME  mail._domainkey.mm.alternatiba.girofle.cloud.
_dmarc.chat            3600  IN CNAME  _dmarc.mm.alternatiba.girofle.cloud.
```

NB : Les courriels doivent être envoyés depuis le domaine `@chat.alternatiba.eu`

Archives RocketChat :
```
chat-archives          3600  IN CNAME  chat-alternatiba-eu.liiib.re.
```
